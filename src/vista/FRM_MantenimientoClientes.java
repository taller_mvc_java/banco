/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.ConexionBD;
import controlador.Controlador_FRM_MantenimientoClientes;
import java.awt.Component;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import modelo.Cliente;

/**
 *
 * @author Usuario
 */
public class FRM_MantenimientoClientes extends javax.swing.JFrame {

    /**
     * Creates new form FRM_MantenimientoClientes
     */
    Controlador_FRM_MantenimientoClientes controlador_FRM_MantenimientoClientes;
    DefaultTableModel modelo;
    public FRM_MantenimientoClientes(ConexionBD conexion) {
        initComponents();
        //this.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{textField, textField_2, textField_1}));
        setLocationRelativeTo(null);
        controlador_FRM_MantenimientoClientes = new Controlador_FRM_MantenimientoClientes(this, conexion);
        jp_Botones.agregarControlador(controlador_FRM_MantenimientoClientes);
        estadoInicial();
        modelo=new DefaultTableModel();
        
        String columnas[]=new String[4];
        columnas[0]="NÚMERO DE CUENTA";
        columnas[1]="TIPO";
        columnas[2]="SALDO";
        columnas[3]="ESTADO";
        modelo.setColumnIdentifiers(columnas);
        jt_Cedula.addKeyListener(controlador_FRM_MantenimientoClientes);
    }
    public void estadoInicial()
    {
        jt_Cedula.setEnabled(true);
        jt_Nombre.setEnabled(false);
        jt_PrimerApellido.setEnabled(false);
        jt_SegundoApellido.setEnabled(false);
        jftf_fechaNacimiento.setEnabled(false);
        jt_Correo.setEnabled(false);
        jt_Provincia.setEnabled(false);
        jt_Cantón.setEnabled(false);
        jt_Distrito.setEnabled(false);
        jta_DireccionExacta.setEnabled(false);
        jt_TelefonoFijo.setEnabled(false);
        jt_TelefonoMovil.setEnabled(false);
        jt_Nacionalidad.setEnabled(false);
        jt_Calidad.setEnabled(false);
        jt_TipoCliente.setEnabled(false);
        
        jt_Cedula.setText("");
        jt_Nombre.setText("");
        jt_PrimerApellido.setText("");
        jt_SegundoApellido.setText("");
        jftf_fechaNacimiento.setText("0000-00-00");
        jt_Correo.setText("");
        jt_Provincia.setText("");
        jt_Cantón.setText("");
        jt_Distrito.setText("");
        jta_DireccionExacta.setText("");
        jt_TelefonoFijo.setText("");
        jt_TelefonoMovil.setText("");
        jt_Nacionalidad.setText("");
        jt_Calidad.setText("");
        jt_TipoCliente.setText("");
        jp_Botones.estadoInicial();
    }
    public int devolverCedula()
    {
        return Integer.parseInt(jt_Cedula.getText());
    }
    public void mostrarInformacion(Cliente cliente)
    {
        jt_Nombre.setText(cliente.getNombre());
        jt_PrimerApellido.setText(cliente.getPrimerApellido());
        jt_SegundoApellido.setText(cliente.getSegundoApellido());
        jftf_fechaNacimiento.setText(""+cliente.getFecha());
        jt_Correo.setText(cliente.getCorreo());
        jt_Provincia.setText(cliente.getProvincia());
        jt_Cantón.setText(cliente.getCanton());
        jt_Distrito.setText(cliente.getDistrito());
        jta_DireccionExacta.setText(cliente.getDireccionExacta());
        jt_TelefonoFijo.setText(""+cliente.getTelefonoFijo());
        jt_TelefonoMovil.setText(""+cliente.getTelefonoMovil());
        jt_Nacionalidad.setText(cliente.getNacionalidad());
        jt_Calidad.setText(cliente.getCalidad());
        jt_TipoCliente.setText(cliente.getTipoCliente());
    }
    public void habilitarAgregar()
    {
        jt_Cedula.setEnabled(false);
        jt_Nombre.setEnabled(true);
        jt_PrimerApellido.setEnabled(true);
        jt_SegundoApellido.setEnabled(true);
        jftf_fechaNacimiento.setEnabled(true);
        jt_Correo.setEnabled(true);
        jt_Provincia.setEnabled(true);
        jt_Cantón.setEnabled(true);
        jt_Distrito.setEnabled(true);
        jta_DireccionExacta.setEnabled(true);
        jt_TelefonoFijo.setEnabled(true);
        jt_TelefonoMovil.setEnabled(true);
        jt_Nacionalidad.setEnabled(true);
        jt_Calidad.setEnabled(true);
        jt_TipoCliente.setEnabled(true);
        jp_Botones.habilitarAgregar();
    }
    public Cliente devolverInformacion() 
    {
        Cliente cliente=null;
        
        cliente=new Cliente(Integer.parseInt(jt_Cedula.getText()),jt_Nombre.getText(),jt_PrimerApellido.getText(),jt_SegundoApellido.getText(),jftf_fechaNacimiento.getText(),jt_Correo.getText(),jt_Provincia.getText(),jt_Cantón.getText(),jt_Distrito.getText(),jta_DireccionExacta.getText(),Integer.parseInt(jt_TelefonoFijo.getText()),Integer.parseInt(jt_TelefonoMovil.getText()),jt_Nacionalidad.getText(),jt_Calidad.getText(),jt_TipoCliente.getText());
        
        return cliente;
    }
    public void habilitarModificarEliminar()
    {
        jp_Botones.habilitarModificarEliminar();
        jt_Cedula.setEnabled(false);
        jt_Nombre.setEnabled(true);
        jt_PrimerApellido.setEnabled(true);
        jt_SegundoApellido.setEnabled(true);
        jftf_fechaNacimiento.setEnabled(true);
        jt_Correo.setEnabled(true);
        jt_Provincia.setEnabled(true);
        jt_Cantón.setEnabled(true);
        jt_Distrito.setEnabled(true);
        jta_DireccionExacta.setEnabled(true);
        jt_TelefonoFijo.setEnabled(true);
        jt_TelefonoMovil.setEnabled(true);
        jt_Nacionalidad.setEnabled(true);
        jt_Calidad.setEnabled(true);
        jt_TipoCliente.setEnabled(true);        
    }
    public void mostrarMensaje(String mensaje)
    {
        JOptionPane.showMessageDialog(null,mensaje);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jl_Cedula = new javax.swing.JLabel();
        jt_Cedula = new javax.swing.JTextField();
        jl_Nombre = new javax.swing.JLabel();
        jt_Nombre = new javax.swing.JTextField();
        jl_PrimerApellido = new javax.swing.JLabel();
        jt_PrimerApellido = new javax.swing.JTextField();
        jl_SegundoApellido = new javax.swing.JLabel();
        jt_SegundoApellido = new javax.swing.JTextField();
        jl_FechaDeNacimiento = new javax.swing.JLabel();
        jftf_fechaNacimiento = new javax.swing.JFormattedTextField();
        jl_Correo = new javax.swing.JLabel();
        jt_Correo = new javax.swing.JTextField();
        jl_Provincia = new javax.swing.JLabel();
        jt_Provincia = new javax.swing.JTextField();
        jl_Provincia1 = new javax.swing.JLabel();
        jt_Cantón = new javax.swing.JTextField();
        jl_Distrito = new javax.swing.JLabel();
        jt_Distrito = new javax.swing.JTextField();
        jl_DireccionExacta = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jta_DireccionExacta = new javax.swing.JTextArea();
        jl_TelefonoFijo = new javax.swing.JLabel();
        jt_TelefonoFijo = new javax.swing.JTextField();
        jl_TelefonoMovil = new javax.swing.JLabel();
        jt_TelefonoMovil = new javax.swing.JTextField();
        jl_Nacionalidad = new javax.swing.JLabel();
        jt_Nacionalidad = new javax.swing.JTextField();
        jl_Nacionalidad1 = new javax.swing.JLabel();
        jt_Calidad = new javax.swing.JTextField();
        jt_TipoCliente = new javax.swing.JTextField();
        jl_Nacionalidad2 = new javax.swing.JLabel();
        jp_Botones = new vista.JP_Botones();
        jl_Fondo = new javax.swing.JLabel();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jl_Cedula.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Cedula.setForeground(new java.awt.Color(0, 0, 204));
        jl_Cedula.setText("Cédula:");
        getContentPane().add(jl_Cedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 120, -1, -1));

        jt_Cedula.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jt_Cedula.setText("101230234");
        getContentPane().add(jt_Cedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 120, 220, -1));

        jl_Nombre.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Nombre.setForeground(new java.awt.Color(0, 0, 204));
        jl_Nombre.setText("Nombre:");
        getContentPane().add(jl_Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 180, -1, -1));

        jt_Nombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 180, 220, -1));

        jl_PrimerApellido.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_PrimerApellido.setForeground(new java.awt.Color(0, 0, 204));
        jl_PrimerApellido.setText("Primer Apellido: ");
        getContentPane().add(jl_PrimerApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 240, -1, -1));

        jt_PrimerApellido.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_PrimerApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 240, 220, -1));

        jl_SegundoApellido.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_SegundoApellido.setForeground(new java.awt.Color(0, 0, 204));
        jl_SegundoApellido.setText("Segundo Apellido: ");
        getContentPane().add(jl_SegundoApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 300, -1, -1));

        jt_SegundoApellido.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_SegundoApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 300, 220, -1));

        jl_FechaDeNacimiento.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_FechaDeNacimiento.setForeground(new java.awt.Color(0, 0, 204));
        jl_FechaDeNacimiento.setText("Fecha de Nacimiento:");
        getContentPane().add(jl_FechaDeNacimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 360, -1, -1));

        jftf_fechaNacimiento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("yyyy-MM-dd"))));
        getContentPane().add(jftf_fechaNacimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 360, 220, -1));

        jl_Correo.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Correo.setForeground(new java.awt.Color(0, 0, 204));
        jl_Correo.setText("Correo:");
        getContentPane().add(jl_Correo, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 410, 150, 40));

        jt_Correo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_Correo, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 420, 220, -1));

        jl_Provincia.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Provincia.setForeground(new java.awt.Color(0, 0, 204));
        jl_Provincia.setText("Provincia:");
        getContentPane().add(jl_Provincia, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 460, 130, 40));

        jt_Provincia.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_Provincia, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 470, 220, -1));

        jl_Provincia1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Provincia1.setForeground(new java.awt.Color(0, 0, 204));
        jl_Provincia1.setText("Cantón:");
        getContentPane().add(jl_Provincia1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 510, 150, 40));

        jt_Cantón.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_Cantón, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 520, 220, -1));

        jl_Distrito.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Distrito.setForeground(new java.awt.Color(0, 0, 204));
        jl_Distrito.setText("Distrito:");
        getContentPane().add(jl_Distrito, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 110, 110, 40));

        jt_Distrito.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_Distrito, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 120, 240, -1));

        jl_DireccionExacta.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_DireccionExacta.setForeground(new java.awt.Color(0, 0, 204));
        jl_DireccionExacta.setText("Dirección Exacta:");
        getContentPane().add(jl_DireccionExacta, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 170, 130, 40));

        jta_DireccionExacta.setColumns(20);
        jta_DireccionExacta.setRows(5);
        jScrollPane1.setViewportView(jta_DireccionExacta);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 180, 240, 90));

        jl_TelefonoFijo.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_TelefonoFijo.setForeground(new java.awt.Color(0, 0, 204));
        jl_TelefonoFijo.setText("Teléfono Fijo:");
        getContentPane().add(jl_TelefonoFijo, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 300, -1, -1));

        jt_TelefonoFijo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_TelefonoFijo, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 300, 240, -1));

        jl_TelefonoMovil.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_TelefonoMovil.setForeground(new java.awt.Color(0, 0, 204));
        jl_TelefonoMovil.setText("Teléfono Móvil:");
        getContentPane().add(jl_TelefonoMovil, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 360, -1, -1));

        jt_TelefonoMovil.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_TelefonoMovil, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 360, 240, -1));

        jl_Nacionalidad.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Nacionalidad.setForeground(new java.awt.Color(0, 0, 204));
        jl_Nacionalidad.setText("Nacionalidad:");
        getContentPane().add(jl_Nacionalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 420, -1, -1));

        jt_Nacionalidad.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_Nacionalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 420, 240, -1));

        jl_Nacionalidad1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Nacionalidad1.setForeground(new java.awt.Color(0, 0, 204));
        jl_Nacionalidad1.setText("Calidad:");
        getContentPane().add(jl_Nacionalidad1, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 470, -1, -1));

        jt_Calidad.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_Calidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 470, 240, -1));

        jt_TipoCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jt_TipoCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 520, 240, -1));

        jl_Nacionalidad2.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jl_Nacionalidad2.setForeground(new java.awt.Color(0, 0, 204));
        jl_Nacionalidad2.setText("Tipo de Cliente:");
        getContentPane().add(jl_Nacionalidad2, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 520, -1, -1));
        getContentPane().add(jp_Botones, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 580, 610, 80));

        jl_Fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/iconos/MESA_TRABAJO_MANT_CLIENTE.png"))); // NOI18N
        getContentPane().add(jl_Fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -20, 1000, 740));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JFormattedTextField jftf_fechaNacimiento;
    private javax.swing.JLabel jl_Cedula;
    private javax.swing.JLabel jl_Correo;
    private javax.swing.JLabel jl_DireccionExacta;
    private javax.swing.JLabel jl_Distrito;
    private javax.swing.JLabel jl_FechaDeNacimiento;
    private javax.swing.JLabel jl_Fondo;
    private javax.swing.JLabel jl_Nacionalidad;
    private javax.swing.JLabel jl_Nacionalidad1;
    private javax.swing.JLabel jl_Nacionalidad2;
    private javax.swing.JLabel jl_Nombre;
    private javax.swing.JLabel jl_PrimerApellido;
    private javax.swing.JLabel jl_Provincia;
    private javax.swing.JLabel jl_Provincia1;
    private javax.swing.JLabel jl_SegundoApellido;
    private javax.swing.JLabel jl_TelefonoFijo;
    private javax.swing.JLabel jl_TelefonoMovil;
    private vista.JP_Botones jp_Botones;
    private javax.swing.JTextField jt_Calidad;
    private javax.swing.JTextField jt_Cantón;
    private javax.swing.JTextField jt_Cedula;
    private javax.swing.JTextField jt_Correo;
    private javax.swing.JTextField jt_Distrito;
    private javax.swing.JTextField jt_Nacionalidad;
    private javax.swing.JTextField jt_Nombre;
    private javax.swing.JTextField jt_PrimerApellido;
    private javax.swing.JTextField jt_Provincia;
    private javax.swing.JTextField jt_SegundoApellido;
    private javax.swing.JTextField jt_TelefonoFijo;
    private javax.swing.JTextField jt_TelefonoMovil;
    private javax.swing.JTextField jt_TipoCliente;
    private javax.swing.JTextArea jta_DireccionExacta;
    // End of variables declaration//GEN-END:variables
}
