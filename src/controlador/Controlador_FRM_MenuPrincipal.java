/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import vista.FRM_MantenimientoClientes;
import vista.FRM_MantenimientoUsuario;
import vista.FRM_MenuPrincipal;

/**
 *
 * @author Usuario
 */
public class Controlador_FRM_MenuPrincipal implements ActionListener{
    
    FRM_MenuPrincipal frm_MenuPrincipal;
    FRM_MantenimientoClientes frm_MantenimientoClientes;
    FRM_MantenimientoUsuario frm_MantenimientoUsuario;
    ConexionBD conexion;
    public Controlador_FRM_MenuPrincipal(FRM_MenuPrincipal frm_MenuPrincipal) {
        conexion=new ConexionBD();
        conexion.realizarConexion();
        
        this.frm_MenuPrincipal=frm_MenuPrincipal;
        frm_MantenimientoClientes=new FRM_MantenimientoClientes(conexion);
        frm_MantenimientoUsuario=new FRM_MantenimientoUsuario();
    }    
    
    public void actionPerformed(ActionEvent evento)
    {
        if(evento.getActionCommand().equals("Salir"))
        {
            System.exit(0);
        }
        if(evento.getActionCommand().equals("Login"))
        {
            JOptionPane.showMessageDialog(null,"Hola");
        }
        if(evento.getActionCommand().equals("Clientes"))
        {
            frm_MantenimientoClientes.setVisible(true);
            frm_MantenimientoClientes.estadoInicial();
        }
        if(evento.getActionCommand().equals("Usuarios"))
        {
            frm_MantenimientoUsuario.setVisible(true);
            //frm_MantenimientoUsuario.estadoInicial();
        }
    }
    
}
