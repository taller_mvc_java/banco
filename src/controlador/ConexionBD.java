

package controlador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Cliente;





public class ConexionBD {
    
    Connection con = null;
    Cliente clienteTemporal;
    public ConexionBD()
    {
        
    }
    public void realizarConexion()
    {
        try {
            String userName = "root";
            String password = "";
            String url = "jdbc:mysql://localhost:3306/banco?characterEncoding=latin1&useConfigs=maxPerformance";//
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, userName, password);
            System.out.println("Conexión Realizada");
        } catch (Exception e) {
            System.err.println("Cannot connect to database server");
            System.err.println(e.getMessage());
            e.printStackTrace();
        } 
    }   
    public boolean consultarCliente(int cedula)
    {
        ResultSet rs = null;
        Statement cmd = null;
        boolean encontrado=false;
        try {
                cmd = con.createStatement();
                rs = cmd.executeQuery("SELECT * FROM banco.cliente where cedula='"+cedula+"'");
                while (rs.next()) 
                {
                    encontrado=true;
                    clienteTemporal=new Cliente(cedula,rs.getString("nombre"),rs.getString("primerApellido"),rs.getString("segundoApellido"),rs.getString("fechaNacimiento"),rs.getString("correo"),rs.getString("provincia"),rs.getString("canton"),rs.getString("distrito"),rs.getString("direccionExacta"),rs.getInt("telefonoFijo"),rs.getInt("telefonoMovil"),rs.getString("nacionalidad"),rs.getString("calidad"),rs.getString("tipoCliente"));
                }
                rs.close();
        }
        catch(Exception e)
        {
            System.out.println("SQLException ejecutando sentencia: " + e.getMessage());
        }
        return encontrado;
    }  
    public boolean insertarCliente(Cliente cliente)
    {
        ResultSet rs = null;
        Statement cmd = null;
        boolean ejecuto;
        try {
                cmd = con.createStatement();
                ejecuto = cmd.execute("INSERT INTO `banco`.`cliente` (`cedula`, `nombre`, `primerApellido`, `segundoApellido`, `fechaNacimiento`, `correo`, `provincia`, `canton`, `distrito`, `direccionExacta`, `telefonoFijo`, `telefonoMovil`, `nacionalidad`, `calidad`, `tipoCliente`) VALUES ('" + cliente.getCedula() + "', '" + cliente.getNombre() + "', '" + cliente.getPrimerApellido() + "', '" + cliente.getSegundoApellido() + "', '" + cliente.getFecha() + "', '" + cliente.getCorreo() + "', '" + cliente.getProvincia() + "', '" + cliente.getCanton() + "', '" + cliente.getDistrito() + "', '" + cliente.getDireccionExacta() + "', '" + cliente.getTelefonoFijo() + "', '" + cliente.getTelefonoMovil() + "', '" + cliente.getNacionalidad() + "', '" + cliente.getCalidad() + "', '" + cliente.getTipoCliente() + "');");
                return true; //rs.close();
        }
        catch(Exception e)
        {
            System.out.println("SQLException ejecutando sentencia: " + e.getMessage());
            return false;
        }
    } 
    public boolean modificarCliente(Cliente cliente)
    {
        ResultSet rs = null;
        Statement cmd = null;
        boolean ejecuto;
        try {
                cmd = con.createStatement();
                ejecuto = cmd.execute("UPDATE `banco`.`cliente` SET `nombre` = '" + cliente.getNombre() + "', `primerApellido` = '" + cliente.getPrimerApellido() + "', `segundoApellido` = '" + cliente.getSegundoApellido() + "', `fechaNacimiento` = '" + cliente.getFecha() + "', `correo` = '" + cliente.getCorreo() + "', `provincia` = '" + cliente.getProvincia() + "', `canton` = '" + cliente.getCanton() + "', `distrito` = '" + cliente.getDistrito() + "', `direccionExacta` = '" + cliente.getDireccionExacta() + "', `telefonoFijo` = '" + cliente.getTelefonoFijo() + "', `telefonoMovil` = '" + cliente.getTelefonoMovil() + "', `nacionalidad` = '" + cliente.getNacionalidad() + "', `calidad` = '" + cliente.getCalidad() + "', `tipoCliente` = '" + cliente.getTipoCliente() + "' WHERE (`cedula` = '"+cliente.getCedula()+"');");
                return true;
        }
        catch(Exception e)
        {
            System.out.println("SQLException ejecutando sentencia: " + e.getMessage());
            return false;
        }
    } 
    public boolean eliminarCliente(String cedula)
    {
        ResultSet rs = null;
        Statement cmd = null;
        boolean ejecuto;
        try {
                cmd = con.createStatement();
                ejecuto = cmd.execute("delete from banco.cliente where cedula='"+cedula+"';");
                return true;
        }
        catch(Exception e)
        {
            System.out.println("SQLException ejecutando sentencia: " + e.getMessage());
            return false;
        }
    } 
    public Cliente getClienteTemporal() {
        return clienteTemporal;
    }
    
}
