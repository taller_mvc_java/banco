/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import modelo.RegistroCliente;
import vista.FRM_MantenimientoClientes;

/**
 *
 * @author Usuario
 */
public class Controlador_FRM_MantenimientoClientes implements ActionListener, KeyListener{

    FRM_MantenimientoClientes frm_MantenimientoClientes;
    RegistroCliente registroCliente;
    ConexionBD conexion;
    int opcion;
    
    public Controlador_FRM_MantenimientoClientes(FRM_MantenimientoClientes frm_MantenimientoClientes, ConexionBD conexion) {
        this.frm_MantenimientoClientes=frm_MantenimientoClientes;
        this.conexion=conexion;
        registroCliente=new RegistroCliente();
    }    
    public void actionPerformed(ActionEvent evento)
    {
        if(evento.getActionCommand().equals("Consultar"))
        {
            consultar();
        }
        if(evento.getActionCommand().equals("Agregar"))
        {
            conexion.insertarCliente(frm_MantenimientoClientes.devolverInformacion());
            frm_MantenimientoClientes.mostrarMensaje("Información agregada de forma correcta.");
            frm_MantenimientoClientes.estadoInicial();
        }
        if(evento.getActionCommand().equals("Modificar"))
        {
            opcion = JOptionPane.showConfirmDialog(null, "Está a punto de modificar la información ¿Está seguro?", "Pregunta", JOptionPane.YES_NO_OPTION);
            if(opcion==0)
            {
                conexion.modificarCliente(frm_MantenimientoClientes.devolverInformacion());
                frm_MantenimientoClientes.mostrarMensaje("Información modificada de forma correcta.");
            }  
            frm_MantenimientoClientes.estadoInicial();            
        }
        if(evento.getActionCommand().equals("Eliminar"))
        {
            opcion = JOptionPane.showConfirmDialog(null, "Está a punto de eliminar la información ¿Está seguro?", "Pregunta", JOptionPane.YES_NO_OPTION);
            if(opcion==0)
            {
                conexion.eliminarCliente(""+frm_MantenimientoClientes.devolverCedula());
                frm_MantenimientoClientes.mostrarMensaje("Información eliminada de forma correcta.");
            }
            frm_MantenimientoClientes.estadoInicial();           
        }
    }
    public void keyTyped(KeyEvent e) {
        //
    }
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode()==10)
        {
            consultar();
        }
    }
    public void keyReleased(KeyEvent e) {
        //
    }  
    public void consultar()
    {
        if(conexion.consultarCliente(frm_MantenimientoClientes.devolverCedula()))
        {
            frm_MantenimientoClientes.mostrarInformacion(conexion.getClienteTemporal());
            frm_MantenimientoClientes.habilitarModificarEliminar();
        }
        else
        {
            opcion = JOptionPane.showConfirmDialog(null, "El cliente con la cédula consultada no existe ¿Desea agregarlo?", "Pregunta", JOptionPane.YES_NO_OPTION);
            if(opcion==0)
            {
                frm_MantenimientoClientes.habilitarAgregar();
            }
            else
            {
                frm_MantenimientoClientes.estadoInicial();
            }                
        } 
    }
}
