-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2023 at 09:31 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banco`
--
CREATE DATABASE IF NOT EXISTS `banco` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `banco`;

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `cedula` int(15) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `primerApellido` varchar(50) NOT NULL,
  `segundoApellido` varchar(50) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `correo` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `canton` varchar(50) NOT NULL,
  `distrito` varchar(50) NOT NULL,
  `direccionExacta` varchar(200) NOT NULL,
  `telefonoFijo` int(15) NOT NULL,
  `telefonoMovil` int(15) NOT NULL,
  `nacionalidad` varchar(15) NOT NULL,
  `calidad` varchar(15) NOT NULL,
  `tipoCliente` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`cedula`, `nombre`, `primerApellido`, `segundoApellido`, `fechaNacimiento`, `correo`, `provincia`, `canton`, `distrito`, `direccionExacta`, `telefonoFijo`, `telefonoMovil`, `nacionalidad`, `calidad`, `tipoCliente`) VALUES
(101230234, 'Juan Rafael', 'More', 'Porras', '2023-09-01', 'juanitomora@gmail.com', 'Puntarenas', 'Esparza', 'Espiritu Santo', '175m este del parque de Esparza.', 22222222, 88888888, 'Costarricense', 'Ahorro', 'Funcionario Publico'),
(601230234, 'Jose Maria', 'Castro', 'Madriz', '2023-09-05', 'castromadriz@gmail.com', 'Guanacaste', 'Cañas', 'Buenos Aires', '100m norte del ciclo Cabecar', 44444444, 88887777, 'Costarricense', 'Ahorro', 'Funcionario Privado');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cedula`);
--
